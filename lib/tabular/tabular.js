import Tabular from 'meteor/aldeed:tabular';
import { FModels } from '/lib/ModelFiles/ModelFiles.js';


TabularTables = {};

Meteor.isClient && Template.registerHelper('TabularTables', TabularTables);

  TabularTables.myFiles=new Tabular.Table({
  name: "myFiles",
  collection: FModels.collection,
  pageLength: 10,
  language: {
                "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            },
  allow(userId) {
       // Para chequear que el usuario este logeado podemos usar (userId!=null), pues si el usuario
      // no esta logeado deberia arrojar null, segun
      // https://forums.meteor.com/t/how-do-meteor-userid-this-userid-and-meteor-user-work-internally/28043/2
     
      //Chequea que usuario este logeado
      return (userId!=null);
    },
    selector:function() {
    return {userId:Meteor.userId()};
  },
  columns: [

    {data: "name", title: "nombre","width": "50%"},
  //   {data: "userId", title: "propietario", visible:false},
  //   {data: "_id", title: "cod. Imagen", visible:false},
  //   {data: "versions.original.meta.pipeFrom", title: "url", visible:false},
    {
      tmpl: Meteor.isClient && Template.modelsboxTools
    }
    ]
  
  
  });
