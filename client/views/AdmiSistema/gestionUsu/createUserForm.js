import {Schemas} from '/lib/collections/users.js';
import './createUserForm.html';

AutoForm.addHooks('afCreateUser', {


	onError: function(formType, error) {
   console.log("estoy en crear usuario");
    if ((error.errorType && error.errorType === 'Meteor.Error') &&(formType==="method")){
      swal(error.error, error.reason);
    }
  },

 // LLamado despues que autoform actualiza un usuario
	onSuccess: function(formType, result) {

		$('#createUserModal').modal('hide');
		

		swal("El sistema ha creado el usuario");
		
		Modal.hide('TmplModalCreateUser');
	}

});





Template.createUserForm.helpers({

	selectedUserDoc: function(){

		return Meteor.users.findOne({_id: Session.get('usuarioId')});

	},
	traeSchema:function(){
		return Schemas.NuevoUsuario;
	},
    idUsuario:function(){
		return Session.get('usuarioId');
	},
   
});
