//import {solve} from  '/lib/solver/solver.js';
var Timer  = require('easytimer');
var timer = new Timer();
timer.addEventListener('secondsUpdated', function (e) {
    $('#basicUsage2').html(timer.getTimeValues().toString());
});

Template.warehouse.events({

  "click  #resolver": function(){
     //imer.reset();
     timer.stop();
     timer.start();
     Meteor.call('getSchedWH',function (error, result){ 
             if (error){
              swal("Error al conectar con cplex cloud ");
              timer.stop();
             }
             else{
            // var cliente=result;
            // var req, res;
             console.log(result);
             console.log("Ejecutando solucion...");
             //swal("Ejecutando solucion...")
             document.getElementById("btnoculto").removeAttribute("hidden");
             document.getElementById("spinerld").removeAttribute("hidden");

              //solve(['warehouse_cloud.mod', 'warehouse_cloud.dat'], req, res);
              //console.log("rol en cliente: "+ rol)
              //Session.set('rol',rol);
              }
    });
    /*swal("vamos a conectar al servidor para resolver");*/
  },
  "click  #eliminar": function(){

     Meteor.call('getElimina',function (error, result){ 
             if (error){
              swal("Error al conectar con cplex cloud ");
             }
             else{
            // var cliente=result;
            // var req, res;
             console.log("Operacion realizada");
             swal("Operacion de eliminacion realizada");
             document.getElementById("btnoculto").setAttribute("hidden","");
             document.getElementById("textoculto").setAttribute("hidden","");
              //solve(['warehouse_cloud.mod', 'warehouse_cloud.dat'], req, res);
              //console.log("rol en cliente: "+ rol)
              //Session.set('rol',rol);
              }
    });
    /*swal("vamos a conectar al servidor para resolver");*/
  },
  "click  #mostrarR": function(){
    if( document.getElementById("mostrarR").textContent == "Mostrar resultados"){
      document.getElementById("textoculto").removeAttribute("hidden");
      document.getElementById("mostrarR").textContent = "Ocultar resultados";
    }else{
      document.getElementById("textoculto").setAttribute("hidden","");
      document.getElementById("mostrarR").textContent = "Mostrar resultados";
    }
    //if(booleano == false){
      Meteor.call('getResult',function (error,result){
         if (error){
          swal("Error al conectar con cplex cloud ");
         }
         else{
        // var cliente=result;
        // var req, res;
         console.log("Operacion realizada");
       }
      });
      //booleano = true;
    //}
  },
  "click  #deletejob": function(){
    swal({
      title: "¿Estas seguro que deseas eliminar el trabajo?",
      text: "Una vez eliminado, no podrás recuperar este archivo!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        Meteor.call('getEliminajob',function (error,result){
           if (error){
            swal("Error al conectar con cplex cloud ");
           }
           else{
          // var cliente=result;
          // var req, res;
          //swal("Trabajo eliminado");
          swal("Operacion realizada con exito!", "Trabajo eliminado!", "success");
           console.log("Operacion realizada");
           timer.stop();
           document.getElementById("btnoculto").setAttribute("hidden","");
           document.getElementById("spinerld").setAttribute("hidden","");
         }
        });
      } else {
        //swal("Your imaginary file is safe!");
      }
    });
  },
});
