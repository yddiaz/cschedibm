

// Licensed under the Apache License. See footer for details.
 /*eslint-env node*/

//For docloud node client

var docloud = require("docplexcloud-nodejs-api");
var fs = require('fs');
var client;
var jobidxd = null;

// initialize the client SDK
function iniciaSolver(){
client = new docloud({
    url: Meteor.settings.private.credentials.url,
    clientId: Meteor.settings.private.credentials.client_id
});
console.log("server starting on " );
return  client;
}


/**
 * Launches a solve for the specified data files.
 * <br>The id of the job created for the solve process is provided with the id field of the JSon object
 * returned with the Http response. This id can later be used by the cliend for requesting info for this job
 * with the './job/:jobid' entry point or delete the job with the '/delete/:jobid' entry point.
 * @param files array of filenames relative to the //home/daniel/csched/resources/ directory.
 * @param req the Http request object.
 * @param res the Http response object.
 */
export function solve(files, req, res) {
  // Response will be returning JSon data.
 // res.setHeader('Content-Type', 'application/json');
 
  var attachments = [];
  files.map(function (file) {
      attachments.push({
          name: file,
          stream: fs.createReadStream('/home/fernando/csched/resources/' + file)
      });

  });
  client.execute({
        logstream: process.stdout,
        parameters: {"oaas.TIME_LIMIT": 3 * 60 * 1000},
        attachments: attachments
    })
    .on('created', function (jobid) {
      jobidxd = jobid;
      console.log(jobid+" created----------------------------")
        // res.writeHead("200");
        // res.write(JSON.stringify({ id: jobid }));
        // res.end();
    })
    .on('processed', function (jobid) {
        console.log(jobid + " processed----------------------------");
        client.downloadAttachment(jobid, 'solution.json', fs.createWriteStream('/home/fernando/csched/resources/solution.json'))
          .then(function () {
              //document.getElementById("cositoxd").setAttribute("hidden","");
              
              client.downloadLog(jobid, fs.createWriteStream('/home/fernando/csched/resources/solution.log'));
              console.log("ya se descargo");
          });

          console.log(jobidxd);
    })
    .on('interrupted', function (jobid) {
        console.log("job was interrupted :"+jobid);
    })
    .on('failed', function (jobid) {
        console.log("job has failed :"+jobid);
    })
    .on('error', function (error) {
        console.log(error);
        res.status(500).send({
            error: true,
            message: error.message
        });
    });
  console.log('works-----------------------');
}

// Metodo que retorna el cliente SDK actual o crea uno sino existe y lo retorna
Meteor.methods({
      getSchedWH:function(){
        if (client===undefined){
          console.log("entrando por null");
            client= iniciaSolver();
     //       console.log (client);
            
        }
        else{ console.log ("cliente ya instanciado");
       //console.log (client);
         
        }
        var req, res;
        var path = process.cwd();
        console.log("camino",path);
//        const text = fs.readFileSync(`/home/daniel/csched/resources/warehouse_cloud.mod`, 'utf8');
  //      console.log(text);
        solve(['warehouse_cloud.mod', 'warehouse_cloud.dat'], req, res);
        
      },
});

// Metodo que retorna el cliente SDK actual o crea uno sino existe y lo retorna
Meteor.methods({
      getSchedSM:function(){
        if (client===undefined){
          console.log("entrando por null");
            client= iniciaSolver();
     //       console.log (client);
            
        }
        else{ console.log ("cliente ya instanciado");
       //console.log (client);
         
        }
        var req, res;
        var path = process.cwd();
        console.log("camino",path);
//        const text = fs.readFileSync(`/home/daniel/csched/resources/warehouse_cloud.mod`, 'utf8');
  //      console.log(text);
        solve(['steelmill.mod', 'steelmill.dat'], req, res);
        
      },
});

//
Meteor.methods({
      getElimina:function(){
        if (client===undefined){
          console.log("entrando por null");
            client= iniciaSolver();
     //       console.log (client);
        }
        else{ console.log ("cliente ya instanciado");
       //console.log (client);
        }
        var req, res;
        var path = process.cwd();
        console.log("camino",path);
        client.deleteJobs()
//        const text = fs.readFileSync(`/home/daniel/csched/resources/warehouse_cloud.mod`, 'utf8');
  //      console.log(text);
          //res.setHeader('Content-Type', 'text/html');

      },
});

export function waitresultjob(jobid){
  client.getJob(jobid).then(function (job) {
      //console.log(JSON.stringify(job));
      //console.log(job);
      if (['PROCESSED', 'FAILED', 'INTERRUPTED'].indexOf(job.executionStatus) < 0) {
          // Job is not started or running: ask for the job status again in 500 millis
          console.log("################sigue esperando");
          setTimeout(waitresultjob(jobidxd), 500);
      }
      else { // Job is done
          console.log("-----------------------------------------trabajo ejecutado");
          document.getElementById("spinerld").setAttribute("hidden","");
          //document.getElementById("textito").value += 'alohaaaaaaaaaaaaaaaaaaa???'
          return "trabajo ejecutado";
      }
      res.writeHead(200);
      res.write(JSON.stringify(job));
      res.end();
  }, function (err) {
      res.send("500",err);
      console.log(err);
  });
}

Meteor.methods({
  getResult:function(result){
       if (client===undefined){
         console.log("entrando por null");
           client= iniciaSolver();
    //       console.log (client);
       }
       else{ console.log ("cliente ya instanciado");
      //console.log (client);
       }
       if(jobidxd != null){
        return waitresultjob(jobidxd);
      }else console.log("/////////////////jobid aun no creado");

  },
});

Meteor.methods({
      getEliminajob:function(){
      //console.log(jobidxd);
       client.deleteJob(jobidxd)
         .then(function (results) {
             console.log("Trabajo eliminado");
             res.send("200");
             res.end();

         }, function (err) {
           res.send("500",err);
           res.end();
       });
    },
});

Meteor.methods({
  getList:function(){
       if (client===undefined){
        console.log("entrando por null");
           client= iniciaSolver();
    //       console.log (client);
           
       }
       else{ console.log ("cliente ya instanciado");
      //console.log (client);
        
       }
      var jobList = {};
      //the job list from docplexcloud client api
      var jobs = client.listJobs();
      //process the promise output, to get the job ids
      jobs.then(function (results) {
          if (results.length === 0) {
              console.log('Actualmente no se encuentran trabajos cargados');
              res.write('You currently have no jobs');
              res.end();
          }
          else {
              console.log('Lista de trabajos: ');
              for (job = 0; job < results.length; job++) {
                  console.log(results[job]._id);
                  jobList[job] = results[job]._id;
              }
              //format the output response to html
              res.writeHead(200, {
                  'Content-Type': 'application/json'
                  , 'Transfer-Encoding': 'chunked'
              });
              res.write(JSON.stringify(jobList));
              res.end();
              console.log("joblist : sent");
          }
          //in case the response fails
      }, function (err) {
          console.log('job list ko' + err);
          res.setHeader('Content-Type', 'application/json');
          res.write("No Jobs to display");
          res.end();
    });
  },
});

